// Query Operators

// Comparison Operators

// $gt / $gte Operator
// SYNTAX: db.collectionName.find({field: {$gt: value}})
db.users.find({age: {$gte: 76}})
db.users.find({age: {$gt: 76}})

// $lt / $lte Operator
db.users.find({age: {$lte: 76}})
db.users.find({age: {$lt: 76}})

// Not equal operator $ne
// SYNTAX: db.collectionName.find({field: {$ne: value}})
db.users.find({age: {$ne: 76}})

// $in operator
// it allows us to find documents with specific match criteria
// SYNTAX: db.collectionName.find({field: {$in: value}})
db.users.find({courses: {$in: ["Sass", "React"]}})

// logical query operator
// or
// SYNTAX: db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}]})
db.users.find({$or: [{firstName: "Neil"}, {age: 25}]})
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt:30}}]})

// and
// SYNTAX: db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]})
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]})

// Evaluation Query Operator
// $regex operator -> used to look for a partial match in a given field
// -> it is used to search for STRINGS in collection
// CASE SENSITIVE
// SYNTAX: db.collectionName.find({field: {$regex: 'pattern/keyword', $options: '$i'}})
db.users.find({firstName: {$regex: 'n'}})
db.users.find({firstName: {$regex: 'N'}})

// make case insensitive: $options: '$i' -> specifies that we want to carry out search without considering letter cases
db.users.find({firstName: {$regex: 'j', $options: '$i'}})


// Field Projection
// if you do not want to retrieve all the fields/properties of a document, you can use field projection

// inclusion
// include/add fields only when retrieving documents
// value of 1 -> to denote that the field is being included
// SYNTAX: db.collectionName.find({criteria}, {field: 1})
db.users.find({firstName: "Jane"}, {
    firstName: 1
    })

// exclusion
// value of 0 -> to denote exclusion of a field
// SYNTAX: db.collectionName.find({criteria}, {field: 0})
db.users.find({firstName: "Jane"}, {
    firstName: 1,
    _id: 0
    })

db.users.find({firstName: "Jane"}, {
   contact: 0,
   department: 0
    })

// if embedded document
db.users.find({firstName: "Jane"}, {
   "contact.phone": 1,
    _id: 0
    })

// slice
db.users.find({}, {
    _id: 0,
    courses: {$slice: [1,2]}
    })